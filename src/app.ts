import {HttpClient, json} from 'aurelia-fetch-client';

let httpClient = new HttpClient();

interface Arm {
  id: number;
  name: string;
  name_en: string;
  image_url: string;
}

export class App {
  heading: string = 'ARMS Randomizer';
  arms: Arm[] = [];

  getArms() {
    httpClient.fetch('http://localhost:8000/random_arms', { method: 'get' })
      .then(response => response.json())
      .then(data => {
        this.arms = data;
      });
  }
}
